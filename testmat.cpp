#include "upc.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

struct element {
  char a[2];
  char b[3];
  char c;
  char d;
};

int main(int argc, char *argv[]) {
  char *bigboy = new char[46 * 177 * 21 + 1];
  srand(1981);
  clear();
  for (int a = 0; a < 1000; ++a) {
    gotoxy(0, 0);
    int k = 0;
    for (int i = 0; i < 46; ++i) {
      for (int j = 0; j < 176; ++j) {
        sprintf(bigboy + k, "\033[38;5;%02dm\033[48;5;00m%c", j % 16,
                a % 26 + 'a');
        k += 21;
      }
      strcpy(bigboy + k, "\033[38;5;00m\033[48;5;00m\n");
      k += 21;
    }
    bigboy[k] = 0;
    std::cout << bigboy;
  }
  delete[] bigboy;
  clearColor();
  return 0;
}
