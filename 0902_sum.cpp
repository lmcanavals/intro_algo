#include <cmath>
#include <cstdint>
#include <iostream>
#include <string>

using namespace std;

bool isInt(string str) {
  bool digits = false;
  for (int i = 0; i < str.size(); ++i) {
    char ch = str[i];
    if (i == 0 && (ch == '-' || ch == '+')) {
      continue;
    } else if (ch >= '0' && ch <= '9') {
      digits = true;
    } else {
      return false;
    }
  }
  return digits;
}

uint32_t readInt(string txt) {
  string str;
  bool intCorrect = false;
  do {
    cout << txt;
    getline(cin, str);
    intCorrect = isInt(str);
    if (!intCorrect) {
      cout << "Debe ingresar un valor entero.\n";
    }
  } while (!intCorrect);

  return stoi(str);
}

// uint64_t es unsigned long long int
uint64_t *factorial(uint32_t *n) {
  uint64_t *f = new uint64_t;
  uint32_t *i = new uint32_t;
  *f = 1;
  for (*i = 2; *i <= *n; ++*i) {
    *f *= *i;
  }

  delete i;

  return f;
}

double *calc_ea(int32_t *a, uint32_t *k) {
  double *s = new double;
  uint32_t *i = new uint32_t;

  *s = 1;

  for (*i = 1; *i <= *k; ++*i) {
    uint64_t *f = factorial(i);
    *s += pow(*a, *i) / *f;
    delete f;
  }

  delete i;

  return s;
}

void main_menu(int *op) {
  string *input = new string;
  *op = -1;
  do {
    cout << "MENU PRINCIPAL\n";
    cout << "1) Determinar el factorial de un número\n";
    cout << "2) Calcular el valor de ea\n";
    cout << "3) Imprime rombo\n";
    cout << "4) Fin\n";
    cout << "\n Escoja una opción [1..4]: ";
    getline(cin, *input);
    if (isInt(*input)) {
      *op = stoi(*input); // stoi: string to int
      if (*op < 1 || *op > 4) {
        cout << "Opción fuera de rango [1..4]\n";
      }
    } else {
      cout << "Debe Ingresar solo números enteros.\n";
    }
  } while (*op < 1 || *op > 4);
  delete input;
}

void option_factorial() {
  uint32_t *n = new uint32_t;

  *n = readInt("Ingrese N: ");
  uint64_t *f = factorial(n);
  cout << "El factorial de " << *n << " es " << *f << '\n';
  delete f;
  delete n;
}

void option_ea() {
  int32_t *a = new int32_t;
  uint32_t *k = new uint32_t;

  *a = readInt("Ingrese a: ");
  *k = readInt("Ingrese k: ");
  double *ea = calc_ea(a, k);
  cout << "El valor aproximado de e^" << *a << " es " << *ea << '\n';
  delete ea;
  delete a;
  delete k;
}

void line(int *n) {
  int *i = new int;

  for (*i = 0; *i < *n; ++*i) {
    cout << " ";
  }

  delete i;
}

void numline(int *n) {
  int *i = new int;

  for (*i = 0; *i < *n; ++*i) {
    cout << *i + 1 << " ";
  }
  cout << '\n';

  delete i;
}

void option_diamond() {
  uint32_t *n = new uint32_t;
  int *pad = new int;
  int *i = new int;
  int *temp = new int;

  *n = readInt("Ingrese N: ");
  *pad = 120 / 2;
  for (*i = 0; *i < *n; ++*i) {
    *pad -= 2;
    line(pad);
    *temp = *i * 2 + 1;
    numline(temp);
  }
  for (*i = 1; *i < *n; ++*i) {
    *pad += 2;
    line(pad);
    *temp = (*n - *i) * 2 - 1;
    numline(temp);
  }

  delete n;
  delete pad;
  delete i;
  delete temp;
}

int main() {
  int *op = new int;
  do {
    main_menu(op);
    switch (*op) {
    case 1:
      option_factorial();
      break;
    case 2:
      option_ea();
      break;
    case 3:
      option_diamond();
      break;
    }
  } while (*op != 4);

  delete op;

  return 0;
}
