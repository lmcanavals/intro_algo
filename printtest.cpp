import time

lineCount = 100000
line = "this is a test"
summary = ""

cmd = "print"
startTime_s = time.time()
for x in range(lineCount):
    print(line)
t = time.time() - startTime_s
summary += "%-30s:%6.3f s\n" % (cmd, t)

cmd = "bigstr"
bigstr = "this is a tost\n" * 100000
startTime_s = time.time()
print(bigstr)
t = time.time() - startTime_s

summary += "%-30s:%6.3f s\n" % (cmd, t)

# Add a newline to match line outputs above...
line += "\n"


print(summary)
