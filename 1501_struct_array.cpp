#include <iomanip>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

using namespace std;

struct Class {
  string name;
  vector<float> evalWeights;
};

struct Student {
  string name;
  string lastName;
  string birthDate;
};

struct Item {
  Class *cls;
  string professor;
  vector<float> grades;
};

struct Record {
  Student *student;
  vector<Item> classes;
};

struct Archive {
  vector<Student> students;
  vector<Class> classes;
  vector<Record> records;
};

ostream &operator<<(ostream &os, Student &student) {
  os << student.lastName << ", " << student.name;
  return os;
}

char mainMenu() {
  char op;
  do {
    cout << "MENU PRINCIPAL\n"
         << "==============\n"
         << "1. Registrar estudiante\n"
         << "2. Registrar curso\n"
         << "3. Registrar matricula\n"
         << "0. Salir\n\n"
         << " >> Ingrese una opción: ";
    cin >> op;
    cin.ignore();
    if (op < '0' || op > '3') {
      cerr << "Opción incorrecta...\nPresione enter para continuar.\n";
      cin.get();
    }
  } while (op < '0' || op > '3');
  return op;
}
void addStudent(vector<Student> &students) {
  Student student;
  cout << "Registrar estudiante\n";
  cout << "Nombre: ";
  getline(cin, student.name);
  cout << "Apellido: ";
  getline(cin, student.lastName);
  cout << "Fecha denacimiento: ";
  getline(cin, student.birthDate);
  students.push_back(student);
  cout << "Estudiante registrado exitosamente\n";
}

void addClass(vector<Class> &classes) {
  Class cls;
  int n;
  cout << "Registrar clase\n";
  cout << "Nombre: ";
  getline(cin, cls.name);
  cout << "Número de evaluaciones: ";
  cin >> n;
  cout << "Ingrese los pesos separados por espacio: ";
  // TODO validar que sumen 1
  for (int i = 0; i < n; ++i) {
    float weight;
    cin >> weight;
    cls.evalWeights.push_back(weight);
  }
  classes.push_back(cls);
  cout << "Clase registrado exitosamente\n";
}

int findStudent(vector<Student> &students) {
  int idxStudent = -1;
  do {
    string lastName;
    cout << "Ingrese apellido del estudiante para la matrícula: ";
    getline(cin, lastName);
    int k = 0;
    vector<int> found;
    for (int i = 0; i < students.size(); ++i) {
      if (students[i].lastName == lastName) {
        cout << setw(3) << ++k << ": " << students[i] << '\n';
        found.push_back(i);
      }
    }
    if (k == 0) {
      cout << "No se encontró ningún estudiante, intente de nuevo\n";
      continue;
    }
    // TODO volver a mostrar lista de estudiantes sin buscar nuevamente.
    int op;
    do {
      cout << "Seleccione el estudiante o 0 para buscar otro: ";
      cin >> op;
    } while (op < 0 || op > k);
    if (op > 0) {
      idxStudent = found[op - 1];
    }
  } while (idxStudent == -1);
  return idxStudent;
}

int findClass(vector<Class> &classes) {
  int idxClass = -1;
  do {
    string name;
    cout << "Ingrese nombre de la clase para agregar a la matrícula: ";
    cin.ignore();
    getline(cin, name);
    int k = 0;
    vector<int> found;
    for (int i = 0; i < classes.size(); ++i) {
      if (classes[i].name == name) {
        cout << setw(3) << ++k << ": " << classes[i].name << '\n';
        found.push_back(i);
      }
    }
    if (k == 0) {
      cout << "No se encontró ninguna clase, intente de nuevo\n";
      continue;
    }
    // TODO volver a mostrar lista de estudiantes sin buscar nuevamente.
    int op;
    do {
      cout << "Seleccione la clase o 0 para buscar otra: ";
      cin >> op;
    } while (op < 0 || op > k);
    if (op > 0) {
      idxClass = found[op - 1];
    }
  } while (idxClass == -1);
  return idxClass;
}

void addRecord(Archive &archive) {
  // TODO validar que se haya registrado por lo menos un alumno y curso.
  Record record;
  char more;
  record.student = &archive.students[findStudent(archive.students)];
  do {
    int idxClass = findClass(archive.classes);
    bool repeated = false;
    for (int i = 0; i < record.classes.size(); ++i) {
      if (record.classes[i].cls == &archive.classes[idxClass]) { // compara dirs
        repeated = true;
        break;
      }
    }
    if (!repeated) {
      string professor;
      cout << "Ingrese el nombre del profesor: ";
      getline(cin, professor);
      cin.ignore();
      record.classes.push_back({&archive.classes[idxClass], professor, {}});
    }
    cout << "Agregar otro (S/N)? ";
    cin >> more;
  } while (more == 'y');
  archive.records.push_back(record);
}

void handleStudents(Archive &archive) { addStudent(archive.students); }

void handleClasses(Archive &archive) { addClass(archive.classes); }

void handleRecords(Archive &archive) { addRecord(archive); }

int main(int argc, char *argv[]) {
  Archive archive;
  char op;
  do {
    op = mainMenu();
    switch (op) {
    case '0':
      cout << "Bye bye!\n";
      break;
    case '1':
      handleStudents(archive);
      break;
    case '2':
      handleClasses(archive);
      break;
    case '3':
      handleRecords(archive);
      break;
    }
  } while (op != '0');

  return 0;
}
