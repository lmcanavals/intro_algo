#include "upc.h"
#include <cctype>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <string>

using namespace std;

const int COLLISION_PARED = 0;
const int CHANGE = 1;
const int COLLISION_ENEMY = 2;

void readMap(string mfname, uint32_t **&map, uint32_t &rows, uint32_t &cols,
             uint32_t &numglyphs, uint32_t *&glyphs, uint32_t *&fgcolors,
             uint32_t *&bgcolors) {
  ifstream file(mfname);
  if (!file.is_open()) {
    cerr << "No se pudo abrir el archivo " << mfname << '\n';
    exit(1);
  }
  file >> rows >> cols >> numglyphs;
  glyphs = new uint32_t[numglyphs];
  fgcolors = new uint32_t[numglyphs];
  bgcolors = new uint32_t[numglyphs];
  for (int i = 0; i < numglyphs; ++i) {
    file >> glyphs[i];
  }
  for (int i = 0; i < numglyphs; ++i) {
    file >> fgcolors[i];
  }
  for (int i = 0; i < numglyphs; ++i) {
    file >> bgcolors[i];
  }
  map = new uint32_t *[rows];
  for (int i = 0; i < rows; ++i) {
    map[i] = new uint32_t[cols];
    for (int j = 0; j < cols; ++j) {
      file >> map[i][j];
    }
  }
}

void printMap(uint32_t **map, uint32_t rows, uint32_t cols, uint32_t numglyphs,
              string glyphs, uint32_t *fgcolors, uint32_t *bgcolors) {
  for (int i = 0; i < rows; ++i) {
    for (int j = 0; j < cols; ++j) {
      uint32_t m = map[i][j];
      if (m != 0) {
        gotoxy(j, i);
        color(fgcolors[m], bgcolors[m]);
        cout << glyphs[m];
      }
    }
  }
  cout.flush();
}

void deleteObj(uint32_t **map, int x, int y, string obj[3]) {
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < obj[i].size(); ++j) {
      char ch = obj[i][j];
      if (ch != ' ') {
        map[y + i][x + j] = 0;
      }
    }
  }
}

void printObj(int x, int y, string obj[3], int pos, uint32_t *fgcolors,
              uint32_t *bgcolors, string glyphs) {
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (obj[i][j] != ' ') {
        gotoxy(x + j, y + i);
        color(fgcolors[pos], bgcolors[pos]);
        cout << glyphs[pos];
      }
    }
  }
}

int moveHero(uint32_t **map, int rows, int cols, string hero[3], int &x, int &y,
             uint32_t *fgcolors, uint32_t *bgcolors, string glyphs2, char key) {
  int nx = x, ny = y;
  switch (key) {
  case 'W':
    --ny;
    break;
  case 'A':
    --nx;
    break;
  case 'S':
    ++ny;
    break;
  case 'D':
    ++nx;
    break;
  }
  // colision! si hay colisión con pared, no se mueve
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < hero[i].size(); ++j) {
      char ch = hero[i][j];
      if (ch != ' ' && map[ny + i][nx + j] == 1) { // COLISION!!
        return COLLISION_PARED;
      }
    }
  }
  // TOOD: validar colisión con enemigo!

  // no colisiones detectadas
  x = nx;
  y = ny;
  return CHANGE;
}

int main() {
  uint32_t **map;
  uint32_t rows;
  uint32_t cols;
  uint32_t numglyphs;
  uint32_t *glyphs;
  string glyphs2 = " O  +@.*πs";
  uint32_t *fgcolors;
  uint32_t *bgcolors;

  bool gameover = false, change = true;
  char key;
  // hero
  int x = 4, y = 1;
  string hero[3] = {"55 ", "555", "55 "};

  clear();
  hideCursor();
  noecho();
  readMap("mapita.gamejam", map, rows, cols, numglyphs, glyphs, fgcolors,
          bgcolors);
  deleteObj(map, x, y, hero);
  while (!gameover) {
    if (_kbhit()) {
      key = toupper(_getch());
      switch (key) {
      case 'W':
      case 'A':
      case 'S':
      case 'D': {
        int res = moveHero(map, rows, cols, hero, x, y, fgcolors, bgcolors,
                           glyphs2, key);
        switch (res) {
        case CHANGE:
          change = true;
          break;
        }
        break;
      }
      case 27:
        gameover = true;
        break;
      }
    }
    if (change) {
      clear();
      clearColor();
      printMap(map, rows, cols, numglyphs, glyphs2, fgcolors, bgcolors);
      clearColor();
      printObj(x, y, hero, 5, fgcolors, bgcolors, glyphs2);
      change = false;
    }
    cout.flush();
    sleep4(100);
  }
  resetAll();
  for (int i = 0; i < rows; ++i) {
    delete[] map[i];
  }
  delete[] map;
  delete[] glyphs;
  delete[] fgcolors;
  delete[] bgcolors;

  return 0;
}
