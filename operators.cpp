/** Ejemplo de sobrecarga de operadores */

#include <iostream>
using namespace std;

struct Point_t {
  int x;
  int y;
};

ostream &operator<<(ostream &os, Point_t &a) {
  os << "(" << a.x << ", " << a.y << ")";
  return os;
}

Point_t operator+(Point_t &a, Point_t &b) {
  Point_t c;
  c.x = a.x + b.x;
  c.y = a.y + b.y;
  return c;
}

int main(int argc, char *argv[]) {
  Point_t a{4, 5};
  Point_t b{7, 3};
  Point_t c;

  cout << "El punto a es: " << a << '\n';
  cout << "El punto b es: " << b << '\n';

  c = a + b;
  cout << a << " + " << b << " = " << c << '\n';

  return 0;
}
