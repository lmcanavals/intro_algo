#include "upc.h"

using namespace std;

// comentario

void Generar_numeros(int *a, int *b, int *c, int *d) {
	*a = randint(1, 41);
	do {
		*b = randint(1, 41);
	} while (*a == *b);
	do {
		*c = randint(1, 41);
	} while (*a == *c || *b == *c);
	do {
		*d = randint(1, 41);
	} while (*a == *d || *b == *d || *c == *d);
}

int main() {
	int *a = new int;
	int *b = new int;
	int *c = new int;
	int *d = new int;
	
	Generar_numeros(a, b, c, d);
	
	cout << *a << " " << *b << " " << *c << " " << *d << endl;
	
	delete a;
	delete b;
	delete c;
	delete d;
	
	return 0;
}
