/* Ejemplo para demostrar el uso de estrucutas para un mini sistema
 * de ventas.
 **/

#include <cstdint>
#include <iostream>
#include <string>

using namespace std;

struct Date_t {
  uint8_t Day;
  uint8_t Month;
  uint16_t Year;
};

struct Time_t {
  uint8_t Hour;
  uint8_t Minute;
  uint8_t Second;
  uint16_t Millisecond;
};

struct DateTime_t {
  Date_t Date;
  Time_t Time;
};

struct Client_t {
  string Name;
  string DNI;
};

struct Vendor_t {
  string Name;
  string DNI;
};

struct Product_t {
  string Name;
  uint16_t Quantity;
  double Price;
};

struct Item_t {
  uint16_t Quantity;
  Product_t *Product;
  double Subtotal;
};

struct Invoice_t {
  Client_t *Client;
  Vendor_t *Vendor;
  DateTime_t EmissionDate;
  Item_t *Items;
  double IGV;
  double Total;
};

ostream &operator<<(ostream &os, DateTime_t &DateTime) {
  os << (int)DateTime.Date.Day << "/" << (int)DateTime.Date.Month << "/"
     << (int)DateTime.Date.Year << " " << (int)DateTime.Time.Hour << ":"
     << (int)DateTime.Time.Minute << ":" << (int)DateTime.Time.Second;
  return os;
}

int main(int argc, char *argv[]) { return 0; }
