/** Otro ejemplo más interesante de sobrecarga */

#include <cstdlib>
#include <iostream>
#include <ostream>
using namespace std;

struct DynArrayInt_t {
  int *a;
  int capacity;
  int len;
};

void MkDAI(DynArrayInt_t &a, int cap = 10) {
  a.capacity = cap;
  a.len = 0;
  a.a = new int[cap];
}

void EnsureCapDAI(DynArrayInt_t &a) {
  if (a.len < a.capacity) {
    return;
  }
  int *newA = new int[a.capacity * 1.5];
  for (int i = 0; i < a.len; ++i) {
    newA[i] = a.a[i];
  }
  a.capacity *= 1.5;
  delete[] a.a;
  a.a = newA;
}

void Add2DAI(DynArrayInt_t &a, int e) {
  EnsureCapDAI(a);
  a.a[a.len++] = e;
}

void DestroyDAI(DynArrayInt_t &a) { delete[] a.a; }

ostream &operator<<(ostream &os, DynArrayInt_t &a) {
  os << "[ ";
  for (int i = 0; i < a.len; ++i) {
    os << (i > 0 ? ", " : "") << a.a[i];
  }
  os << " ]";
  return os;
}

DynArrayInt_t operator+(DynArrayInt_t &a, DynArrayInt_t &b) {
  DynArrayInt_t c;
  MkDAI(c);
  for (int i = 0; i < a.len; ++i) {
    Add2DAI(c, a.a[i] + b.a[i]);
  }
  return c;
}

int main(int argc, char *argv[]) {
  DynArrayInt_t a;
  DynArrayInt_t b;
  DynArrayInt_t c;

  MkDAI(a);
  MkDAI(b);

  for (int i = 0; i < 20; ++i) {
    Add2DAI(a, rand() % 100);
    Add2DAI(b, rand() % 100);
  }

  cout << a << " +\n";
  cout << b << " =\n";

  c = a + b;

  cout << c << '\n';

  DestroyDAI(a);
  DestroyDAI(b);
  DestroyDAI(c);

  return 0;
}
